# Note Trainer

Quick hacky flashcards/vocab trainer like webapp I threw together to be able to comfortably and efficiently learn reading notes without playing the piano and falling into playing by ear instead of by sheet.

Try it out in action at https://peternerlich.gitlab.io/notetrainer

## Features

* [X] Show note, check answer
* [X] Generate random note within `n` helper lines of the stave for **treble** and **bass** clef
* [X] Display **stats** (proportion of correct answers, time needed last time/on average/in total)
* [X] Pause timer when window is in background
* [ ] Persistent stats keeping
* [ ] Advanced stats to see progress
* [ ] Configuration window + logic to train specific ranges / without accidentals etc.
* [ ] Cool styling
* [ ] Mobile friendly input
* [ ] More clefs
* [ ] ...