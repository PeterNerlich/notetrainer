
function generate_note() {
	let help_lines = 3;

	let clef = random_of(['treble', 'bass']);
	let note = random_between(0 - 2*help_lines, 9 - 2*help_lines);
	let mod = random_of([-1, 0, 1]);

	let info = note_info(clef, note, mod);

	return {
		name: info.name,
		clef, note, mod, octave: info.octave,
		equals: n=>n.clef==clef&&n.note==note&&n.mod==mod
	};
}

function note_info(clef, note, mod) {
	let s_note = '';
	let s_mod = [];
	let octave = 0;

	let notes = ['c', 'd', 'e', 'f', 'g', 'a', 'b'];
	let offset = 0;

	switch (clef) {
		case 'treble':
			offset = 2;
			octave = 4;
			break;
		case 'bass':
			offset = 4;
			octave = 2;
			break;
		default:
			offset = 0;
	}
	while (note+offset < 0) {
		note += notes.length;
		octave++;
	}
	s_note = notes[(note+offset) % notes.length];


	switch (mod) {
		case -1:
			s_mod.push('♭');
			s_mod.push('b');
			if (['e', 'a'].includes(s_note)) {
				s_mod.push('s');
			} else {
				s_mod.push('es');
			}
			s_mod.push('flat');
			break;
		case 1:
			s_mod.push('♯');
			s_mod.push('#');
			s_mod.push('is');
			s_mod.push('sharp');
			break;
		default:
			s_mod.push('');
			s_mod.push('');
			s_mod.push('');
			s_mod.push('');
	}

	return {
		name: [
			s_note+s_mod[0],
			s_note+s_mod[1],
			s_note+s_mod[2],
			(s_note.toUpperCase()+' '+s_mod[3]).trim()
		],
		octave
	};
}


function random_between(max, min, i) {
	i = i || 1;
	min = min || 0;

	return min + (Math.floor(Math.random() * (max-min+i)/i) * i);
}

function random_of(arr) {
	return arr[random_between(arr.length-1)];
}
