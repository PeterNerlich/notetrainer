
const feedbackTimeout = 2000;


let display = document.getElementById('display');
let result = document.getElementById('result');
let form = document.getElementById('guess');
let input = form.querySelector('input[name="guess"]');
let summary_el = document.getElementById('summary');

form.addEventListener('submit', evaluateAnswer);
window.addEventListener('blur', stopTime);
window.addEventListener('focus', resumeTime);


let currentChallenge = generate_note();
let summary = {
	total: 0,
	correct: 0,
	lasttime: 0,
	avgTime: 0,
	runtime: 0,

	tmpTimestamp: null,
	tmpTime: null
};




// init first question
nextQuestion();






function evaluateAnswer(e) {
	e.preventDefault();

	if (summary.tmpTime) {
		resumeTime();
	}

	if (input.value.trim() == '') {
		// accident/no answer, clear and try again
		input.value = '';
		input.focus();

	} else {

		summary.lasttime = Date.now() - summary.tmpTimestamp;
		summary.runtime += summary.lasttime;
		summary.avgTime = (summary.avgTime*summary.total + summary.lasttime) / (summary.total+1);
		summary.total++;

		if (checkGuess(input.value, currentChallenge)) {
			summary.correct++;
			result.innerText = 'Correct! '+currentChallenge.name[0];
		} else {
			result.innerText = 'Wrong. The correct answer is '+currentChallenge.name[0];
		}

		display_summary();

		setTimeout(nextQuestion, feedbackTimeout);
	}
}


function nextQuestion() {
	// clear answer and result
	input.value = '';
	result.innerText = '';

	// generate new challenge
	let t = currentChallenge;
	while (t.equals(currentChallenge)) {
		t = generate_note();
	}
	currentChallenge = t;

	// show note
	display.innerHTML = '';
	draw_init(display);
	drawNote(currentChallenge);

	summary.tmpTimestamp = Date.now();

	input.focus();
}


function checkGuess(guess, challenge) {
	let normalizedGuess = guess.toLowerCase();
	let normalizedChoices = challenge.name.map(e=>e.toLowerCase());
	return normalizedChoices.includes(normalizedGuess);
}



function display_summary() {
	summary_el.querySelector('span#correct').innerText = summary.correct;
	summary_el.querySelector('span#total').innerText = summary.total;
	summary_el.querySelector('span#lasttime').innerText = Math.round(summary.lasttime/1000);
	summary_el.querySelector('span#avgTime').innerText = Math.round(summary.avgTime/100)/10;
	summary_el.querySelector('span#runtime').innerText = Math.round(summary.runtime/1000);
}



function stopTime() {
	summary.tmpTime = Date.now() - summary.tmpTimestamp;
	summary.tmpTimestamp = null;
}

function resumeTime() {
	summary.tmpTimestamp = Date.now() - summary.tmpTime;
	summary.tmpTime = null;
}

