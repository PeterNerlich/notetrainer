
let VF = Vex.Flow;
let renderer, context;


function draw_init(container) {
	renderer = new VF.Renderer(container, VF.Renderer.Backends.SVG);

	// Size our svg:
	renderer.resize(150, 150);

	// And get a drawing context:
	context = renderer.getContext();
}



function drawNote(challenge) {
	let stave = new VF.Stave(5, 15, 150);
	stave.addClef(challenge.clef);
	stave.setContext(context).draw();

	let notes = [
		new Vex.Flow.StaveNote({ clef: challenge.clef, keys: [challenge.name[1]+'/'+challenge.octave], duration: "q" })
	];
	if (challenge.mod > 0) {
		notes[0].addAccidental(0, new Vex.Flow.Accidental("#"));
	} else if (challenge.mod < 0) {
		notes[0].addAccidental(0, new Vex.Flow.Accidental("b"));
	}

	Vex.Flow.Formatter.FormatAndDraw(context, stave, notes);
}
